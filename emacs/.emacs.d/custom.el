(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (mytheme-dark)))
 ;; '(sml/theme (quote light))
 '(visible-cursor t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(git-gutter+-separator ((t (:foreground "yellow" :weight normal))))
 ;; '(sml/col-number ((t nil)))
 ;; '(sml/filename ((t (:inherit sml/global :foreground "Blue" :weight normal))))
 ;; '(sml/folder ((t (:inherit sml/global :weight normal))))
 ;; '(sml/global ((t (:foreground "black" :inverse-video nil))))
 ;; '(sml/line-number ((t (:inherit sml/modes :weight normal))))
 )
