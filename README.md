##README:

This repository is a copy of common and essential Linux tools I use for my Linux developement at work place and home.


	+------+----------+-------------------------+
	|Sl No |Tool      |Installation target      |
	+------+----------+-------------------------+
	|  1   |Emacs     |$HOME/.emacs             |
	+------+----------+-------------------------+
	|  2   |screen    |$HOME/.screenrc          |
	+------+----------+-------------------------+
	|  3   |tmux      |$HOME/.tmux.conf         |
	+------+----------+-------------------------+
	|  4   |zsh       |$HOME/.zshrc             |
	+------+----------+-------------------------+
	|  5   |vim       |$HOME/.vimrc             |
	+------+----------+-------------------------+
	|  6   |powerline |$HOME/.config/powerline/*|
	+------+----------+-------------------------+
	|  7   |git       |$HOME/.gitconfig         |
	+------+----------+-------------------------+
	|  8   |X         |$HOME/.Xdefaults         |
	+------+----------+-------------------------+

-- End of File --

